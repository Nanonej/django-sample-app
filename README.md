# README

## Requirements
- Python 3.9
- Django 3.2

## Setting up

- Clone the project

- Check Python and Django versions

- Install requirements with:
```console
$ pip3 install -r requirements.txt
```

## Up and running

- Run tests with PyTest:
```console
$ pytest
```

- Start the server with:
```console
$ python3 manage.py runserver
```

- Finally acces the app on `http://127.0.0.1:8000/` and admin on `/admin`
