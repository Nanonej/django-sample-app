from django.urls import path

from . import views

app_name = 'booking'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.ShowView.as_view(), name='show'),
]
