from django.views import generic

from .models import Resource


class IndexView(generic.ListView):
    template_name = 'booking/index.html'

    def get_queryset(self):
        return Resource.objects.all()

class ShowView(generic.DetailView):
    model = Resource
    template_name = 'booking/show.html'
