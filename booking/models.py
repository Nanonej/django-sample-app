from django.db import models

# We could make a Model with ResourceTypes and link it with a foreign key to Resources to allow admin to easily create new types.
class Resource(models.Model):
    class ResourceTypes(models.IntegerChoices):
        MEETING_ROOM = 1
        OPEN_SPACE = 2
        DESK = 3

    label = models.CharField(max_length=50)
    type = models.IntegerField(choices=ResourceTypes.choices, default=ResourceTypes.DESK)
    location = models.CharField(max_length=200)
    capacity = models.PositiveIntegerField()

    def __str__(self):
        return self.label
