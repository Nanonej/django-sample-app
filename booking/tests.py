import pytest

from django.test import Client
from django.urls import reverse

pytestmark = pytest.mark.django_db

class TestIndex:
    def test_get_object(self, client):
        response = client.get(reverse('booking:index'))
        assert response.status_code == 200
        assert response.context
